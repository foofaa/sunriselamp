# SunriseLamp
A LAN-attached LED lamp, controlled via MQTT.

For a far fuller explanation of this project, please see: https://chrismolloy.com/p205.php

## sunriseLamp.py
This is the code that talks to the microcontroller inside the lamp. Note that this code is optional - any MQTT frontend will work.

The C_TRIGGERS array maps a label to a device+action. I've hard-coded this, as it won't change very often, but you could read this in as a JSON file, if that would suit you better.

A 'trigger' includes: a label (CLI argument); the MQTT topic (address of specific lamp instance); the MQTT payload ('b' = brightness target, or "-1" to switch to OTA-mode; 'd' = duration , in seconds; 'p' = number of pulses to finish with; 'f' = final brightness, or "-1" to hold at the target); and an 'always' flag ('false' = don't do this action if the sun is up).

Multiple triggers can be passed into the Python script via the CLI (comma separated): $> python sunriseLamp.py bed1-sunrise,bed2-sunrise

Schedule via cron, e.g.: 0 7 * * * python /home/pi/sunriseLamp.py test <- runs trigger 'test' everyday @ 07h00

Python imports: astral; datetime; paho.mqtt.publish; pytz; socket; sys

## SunriseLamp.cpp
This is the code that runs on the microcontroller inside the lamp. You'll need to update the 'build_flags' in your platformio.ini to match your situation (see below).

I'm using a Adafruit Feather HUZZAH ESP8266 microcontroller, but any module with WiFi and a single PWM GPIO will work.

I'm using an MQTT interface to control the lamp - see code comments for examples. Search the code for '###', and replace, as appropriate.

It should be noted that the microcontroller can be sent a new set of instructions at any time (as opposed to an action being triggered, and you having to wait until its fully complete). Amongst other things, this means you can cancel/switch off any action before it has completed.

PlatformIO includes: ArduinoOTA; ESP8266mDNS; ESP8266WiFi; PubSubClient@2.7

My platformio.ini for this project:

    [env:huzzah]
    platform = espressif8266
    board = huzzah
    framework = arduino
    upload_protocol = espota
    upload_port = 192.168.1.###
    upload_flags = --auth=### YOUR OTA PASSWORD ###
    build_flags =
      -DC_ADDRESS=### 4TH IP ADDRESS BLOCK - AS PER upload_port, ABOVE ###
      -DC_CLIENT=\"### MQTT CLIENT ID ###\"
      -DC_TOPIC=\"### MQTT TOPIC FOR THIS DEVICE ###\"
      -DC_SSID=\"### YOUR WIFI SSID ###\"
      -DC_SSPW=\"### YOUR WIFI PASSWORD ###\"
      -DC_HASH=\"### YOUR OTA PASSWORD HASH, AS PER 'upload_flags = --auth', ABOVE ###\"
    lib_deps =
      ArduinoOTA
      ESP8266mDNS
      ESP8266WiFi
      PubSubClient@2.7

## SVG Assets
Laser-cutting templates.
