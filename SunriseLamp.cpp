//////////////////////////////////////////////////////////////////////////////
// This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
// Title of work: Sunrise Lamp
// Attribute work to name: Chris Molloy
// Attribute work to URL: https://chrismolloy.com/p205.php
// Format of work: Software
// Version: 20190720-1310
//////////////////////////////////////////////////////////////////////////////
// 
// MQTT
// ----
// Topic: iot/sunrise/[room]/set
// Payload: b~d~p~f
// b = brightness_target : [-1, 1023]; "-1" = listen for OTA
// d = duration_ms / 1000 : span of time over which brightness changes
// p = end_action_pulse : [0, n] pulses upon completing primary fade
// f = end_action_final : [-1, 1023]; fade to this over 3-sec; "-1" = brightness_target (i.e. no fade, just hold - auto-off after 15-mins)
// 
// EXAMPLES
// --------
// 'sunrise - full bright': 1023~900~3~0 : 'fade up to full-on over 15-mins (15*60), then pulse 3 times, and finally switch off'
// 'sunrise - half bright': 511~900~3~-1 : 'fade up to half-on over 15-mins (15*60), then pulse 3 times, and finally stay on'
// 'kids bedtime': 511~3~5~0 : 'fade up to half-on over 3-secs, then pulse 5 times, and finally switch off'
// 'off': 0~0~0~0 : 'off immediately' - no cron, mainly for manual override
// 'ota': -1~0~0~0 : switch to 'Over The Air' update mode and hold until Reset (or new trigger)

#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define HUZZAH_LED_RED 0
#define HUZZAH_LED_BLUE 2
#define GPIO_PWM 13
#define MAX_BRIGHTNESS 959 // used to cap maximum brightness (in my case, in order to limit the maximum current draw to 959/1023)
// build_flags =
// -DC_ADDRESS
// -DC_CLIENT
// -DC_TOPIC
// -DC_SSID
// -DC_SSPW
// -DC_HASH

const bool DEBUG = false; // if 'true', Serial.println() status messages

WiFiClient clientWifi;
PubSubClient clientPubSub(clientWifi);

IPAddress ip(192, 168, 1, C_ADDRESS); // ip address of this ESP8266 device
IPAddress gateway(192, 168, 1, 1); // ip address of the router on the same subnet (i.e. 3rd digit in this and previous ip address must match)
IPAddress subnet(255, 255, 255, 0);

unsigned long millis_interval;
float brightness_delta;
float time_scale;
int brightness_start;
int brightness_target;
int end_action_final;
int end_action_pulse;
bool do_end_action;
bool fade_up;

unsigned long millis_previous = 0;
unsigned long millis_current = 0;
int brightness_current = 0;

unsigned long get_dt() {
  float b01 = (float)(fade_up ? (brightness_start - brightness_current) : (brightness_target - brightness_current)) / brightness_delta; // [0, 1)
  float t01 = sqrt(1.0 - (b01 * b01)); // (0, 1]
  return (unsigned long)round(t01 * time_scale);
}

void compute_constants(int b, int d, int p, int f) {
  brightness_start = brightness_current;
  brightness_target = constrain(b, -1, MAX_BRIGHTNESS);
  if (brightness_start == brightness_target) return; // avoid division by zero, below

  end_action_pulse = p;
  end_action_final = constrain(f, -1, MAX_BRIGHTNESS);
  do_end_action = true;

  brightness_delta = (float)(brightness_target - brightness_start);
  time_scale = abs((float)(4000 * d) / (PI * brightness_delta));
  fade_up = brightness_target > brightness_start;
  millis_interval = get_dt();
}

void callback(char* topic, byte* payload, unsigned int length) {
  char sBlank[] = "";
  char *params[4];
  char *ptr = NULL;
  params[0] = sBlank;
  params[1] = sBlank;
  params[2] = sBlank;
  params[3] = sBlank;

  int index = 0;
  payload[length] = '\0'; // NULL terminate (assumes payload in no more than MQTT_MAX_PACKET_SIZE - 1 (= 127) long)
  ptr = strtok((char*)payload, "~");
  while(ptr != NULL) {
    params[index] = ptr;
    index++;
    if (index >= 4) break;
    ptr = strtok(NULL, "~");
  }

  String sB = String(params[0]);
  String sD = String(params[1]);
  String sP = String(params[2]);
  String sF = String(params[3]);

  if (DEBUG) Serial.println(sB);
  if (DEBUG) Serial.println(sD);
  if (DEBUG) Serial.println(sP);
  if (DEBUG) Serial.println(sF);

  compute_constants(sB.toInt(), sD.toInt(), sP.toInt(), sF.toInt());
}

void setup() {
  if (DEBUG) Serial.begin(74880);

  // ### WIFI ###
  ESP.eraseConfig();
  WiFi.persistent(false); // only write credentials to flash if they have changed since last time - see https://github.com/esp8266/Arduino/blob/master/doc/esp8266wifi/generic-class.md#persistent
  WiFi.mode(WIFI_STA); // Station-only mode
  WiFi.config(ip, gateway, subnet);
  WiFi.begin(C_SSID, C_SSPW);

  // ### PWM DRIVER ###
  pinMode(GPIO_PWM, OUTPUT);
  digitalWrite(GPIO_PWM, HIGH); // OFF

  // ### OTA ###
  ArduinoOTA.setPasswordHash(C_HASH);
  ArduinoOTA.onStart([]() {
    if (DEBUG) Serial.println("OTA.onStart()");
  });
  ArduinoOTA.onEnd([]() {
    if (DEBUG) Serial.println("OTA.onEnd()");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    if (DEBUG) Serial.printf("OTA.onProgress(): %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    if (DEBUG) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR)
        Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR)
        Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR)
        Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR)
        Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR)
        Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();

  // ### MQTT ###
  clientPubSub.setServer("192.168.1.###", 1883); // ### YOUR MQTT BROKER HERE ###
  clientPubSub.setCallback(callback);

  // ### DEBUG ###
  if (DEBUG) {
    Serial.println("");
    Serial.print("DEBUG: ");
    Serial.println(WiFi.localIP());
  }

  brightness_current = 0;
  compute_constants(MAX_BRIGHTNESS, 10, 3, 0); // = Demo Mode (10-sec fade up to full brightness; 3 pulses; off)
}

void loop() {
  if (!clientPubSub.connected()) {
    if (clientPubSub.connect(C_CLIENT, "###", "###")) { // ### YOUR MQTT CREDENTIALS (clientID, username, password) HERE ###
      if (clientPubSub.subscribe(C_TOPIC, 1)) { // QoS = 1
        if (DEBUG) Serial.println("loop(): ... subscribe SUCCESS!");
      } else {
        if (DEBUG) Serial.println("loop(): ... subscribe FAILED!");
      }
    }
  }
  clientPubSub.loop();

  if (brightness_target == -1) { // OTA mode
    ArduinoOTA.handle();
  } else { // normal mode
    millis_current = millis();
    if (millis_current < millis_previous) { // clocked, happens about every 50-days
      millis_previous = millis_current;
    } else if (millis_current - millis_previous >= millis_interval) { // enough time has elapsed...
      millis_previous = millis_current;
      if (brightness_current < brightness_target) {
        brightness_current++; // fade up
        analogWrite(GPIO_PWM, 1023 - brightness_current); // invert value!
        millis_interval = get_dt();
      } else if (brightness_current > brightness_target) {
        brightness_current--; // fade down
        analogWrite(GPIO_PWM, 1023 - brightness_current); // invert value!
        millis_interval = get_dt();
      } else { // brightness_current == brightness_target, i.e. finished
        if (do_end_action) { // i.e. values have been changed by callback()
          do_end_action = false;
          if (end_action_pulse > 0 && brightness_current > 0) { // no pulsing if currently fully off
            int ii = 0;
            int kk = brightness_current;
            while (ii++ < end_action_pulse) {
              while (--kk >= 0) { // quick fade down
                analogWrite(GPIO_PWM, 1023 - kk); // invert value!
                delay(1);
              }
              while (++kk <= brightness_current) { // quick fade up
                analogWrite(GPIO_PWM, 1023 - kk); // invert value!
                delay(1);
              }
            }
          }
        } else { // end action (if any) already done
          if (end_action_final < 0) { // hold final brightness (auto-off after 15-mins)
            end_action_final = 0;
            millis_interval = (unsigned long)(900 * 1000); // 15-mins
          } else {
            if (brightness_current == end_action_final) { // hold here...
              millis_interval = (unsigned long)(900 * 1000); // (arbitrary) 15-mins
            } else {
              compute_constants(end_action_final, 3, 0, end_action_final); // quick (3-sec) fade to final value
            }
          }
        }
      }
    }
  }
}

