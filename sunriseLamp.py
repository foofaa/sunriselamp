#!/usr/bin/python
# //////////////////////////////////////////////////////////////////////////////
# // This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
# // Title of work: Sunrise Lamp
# // Attribute work to name: Chris Molloy
# // Attribute work to URL: https://chrismolloy.com/p205.php
# // Format of work: Software
# // Version: 20190629-1514
# //////////////////////////////////////////////////////////////////////////////
#
# Schedule via cron, e.g.: 0 7 * * * python /home/pi/sunriseLamp.py bed2-test <- runs trigger 'bed2-test' everyday @ 07h00

from astral import Astral
from datetime import datetime
import paho.mqtt.publish as publish
import pytz
import sys

C_LOG_ID = 'SUNRISELAMP'
C_ASTRAL_CITY = '### YOUR CITY HERE ###'
C_AUTH = {'username':"### YOUR MQTT USERNAME HERE ###", 'password':"### YOUR MQTT PASSWORD HERE ###"}

C_TRIGGERS = {
    'bed1-off': {'topic': 'iot/sunrise/bed1/set', 'payload': '0~0~0~0', 'always': True}, # off immediately - no cron, mainly for manual override
    'bed1-sunrise': {'topic': 'iot/sunrise/bed1/set', 'payload': '511~900~3~-1', 'always': False}, # fade up to half-on over 15-mins (15*60), then pulse 3 times, and finally stay on - cron @ 06:45
    'bed1-bedtime': {'topic': 'iot/sunrise/bed1/set', 'payload': '511~3~5~0', 'always': True}, # fade up to half-on over 3-secs, then pulse 5 times, and finally switch off - cron @ 20:45 and 21:00
    'bed1-ota': {'topic': 'iot/sunrise/bed1/set', 'payload': '-1~0~0~0', 'always': True}, # switch to 'Over The Air' update mode and hold until Reset (or new trigger)
    'bed1-test': {'topic': 'iot/sunrise/bed1/set', 'payload': '1023~10~3~0', 'always': True}, # fade up to full-on over 10-secs, then pulse 3 times, and finally switch off

    'bed2-off': {'topic': 'iot/sunrise/bed2/set', 'payload': '0~0~0~0', 'always': True}, # as per 'bed1-off'
    'bed2-sunrise': {'topic': 'iot/sunrise/bed2/set', 'payload': '511~900~3~-1', 'always': False}, # as per 'bed1-sunrise'
    'bed2-ota': {'topic': 'iot/sunrise/bed2/set', 'payload': '-1~0~0~0', 'always': True}, # as per 'bed1-ota'
    'bed2-test': {'topic': 'iot/sunrise/bed2/set', 'payload': '1023~10~3~0', 'always': True} # as per 'bed1-test'
}

triggers = ['bed2-test']
if len(sys.argv) > 1:
    triggers = sys.argv[1].split(',') # e.g. $> python sunriseLamp.py bed1-sunrise,bed2-sunrise

astral = Astral()
astral.solar_depression = -3.0 # sun is 3-degrees ABOVE horizon
city = astral[C_ASTRAL_CITY]
now = datetime.now()
now = pytz.timezone(city.timezone).localize(now)
sun = city.sun(local=True)
is_dark = (now < sun['dawn'] or now > sun['dusk'])
print(C_LOG_ID + '/D: ' + sys.argv[1] + ', dawn: ' + str(sun['dawn']))

msgs = []
for trigger in triggers:
    params = C_TRIGGERS['bed2-test']
    if C_TRIGGERS.has_key(trigger):
        params = C_TRIGGERS[trigger]

    if is_dark or params['always']:
        msgs.append({'topic': params['topic'], 'payload': params['payload'], 'qos': 2})
    else:
        print(C_LOG_ID + "/D: ... no action (not dark): " + trigger)
    # end if
# end for

if len(msgs) > 0:
    publish.multiple(msgs, hostname="localhost", port=1883, client_id="sunriseLamp.py", keepalive=60, auth=C_AUTH)
# end for

